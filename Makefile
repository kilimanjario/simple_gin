postgres:
	docker run --name pg12 -p 5432:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=Blead1234 -d postgres:12-alpine
createdb:
	docker exec -it pg12 createdb --username=root --owner=root simple_bank
dropdb:
	docker exec -it pg12 dropdb simple_bank
migrateup:
	migrate -path db/migration -database "postgresql://root:Blead1234@localhost:5432/simple_bank?sslmode=disable" -verbose up
migratedown:
	migrate -path db/migration -database "postgresql://root:Blead1234@localhost:5432/simple_bank?sslmode=disable" -verbose down
.PHONY: postgres createdb dropdb migrateup migratedown
